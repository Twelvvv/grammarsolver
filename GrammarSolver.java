// Erik Zorer, CS 145, Spring 2014, Section A
// Programming Assignment #4 - GrammarSolver, 05/20/2014
/**
 * This class allows a user to generate any amount of random instances of an
 * element of a grammar.
 */
import java.util.*;

public class GrammarSolver {
	private SortedMap<String, String> map;

	/** 
	 * Accepts a list of grammar rules and stores it in a TreeMap.
	 * @param grammar grammar symbols to be stored
	 * @throws IllegalArgumentException - if the list of grammar is empty
	 */
	public GrammarSolver(List<String> grammar) {
		if (grammar.isEmpty())
			throw new IllegalArgumentException();
		map = new TreeMap<String, String>();
		for (String line : grammar) {
			String[] parts = line.split("::=");
			String nonterminal = parts[0],
				   rules = parts[1].trim();
			map.put(nonterminal, rules);
		}
	}

	/** 
	 * Returns true if the given symbol is a nonterminal of the grammar,
	 * otherwise false.
	 * @param symbol symbol to be tested 
	 * @return true if given symbol is a nonterminal of the grammer, otherwise
	 *		   false
	 */
	public boolean grammarContains(String symbol) {
		return map.containsKey(symbol);
	}

	
	/** 
	 * Uses the grammar to randomly generate the given number of occurrences of
	 * the given symbol and returns the result as an array of Strings.  For any
	 * given nonterminal symbol, each of its rules is applied with equal
	 * probability.
	 * @param symbol symbol to generate
	 * @param times number of times to generate the specified symbol
	 * @return array of generated symbols
	 * @throws IllegalArgumentException - if the symbol isn't a nonterminal or
	 *		   the specified number of times is a negative value
	 */
	public String[] generate(String symbol, int times) {
		if (!map.containsKey(symbol) || times < 0)
			throw new IllegalArgumentException();
		String[] results = new String[times];
		for (int i = 0; i < times; i++) {
			results[i] = generate(symbol);
		}
		return results;
	}

	private String generate(String symbol) {
		String s = map.get(symbol).trim();
		String[] parts;
		// if multiple rules (indicated by a "|"), divide into seperate strings
		// and select a rule by random
		if (s.contains("|")) {
			parts = s.split("[|]");
			int random = (int) Math.round((Math.random() * (parts.length - 1)));
			s = parts[random].trim();
		}
		// split rule into tokens; if token is a nonterminal, generate another
		// symbol, otherwise add token to result string
		parts = s.split("[ \t]+");
		String result = "";
		for (int i = 0; i < parts.length; i++) {
			String token = parts[i].trim();
			result += (map.containsKey(token) ? generate(token) : token + " ");
		}
		return result;
	}
	
	/** 
	 * This method returns a String representation of the various nonterminal
	 * symbols from the grammar
	 * @return string representation of nonterminal symbols
	 */
	public String getSymbols() {
		return map.keySet().toString();
	}
}
